#Provider which ask an access_token on service account for imperssonate 
#https://medium.com/wescale/how-to-generate-and-use-temporary-credentials-on-google-cloud-platform-b425ef95a00d
provider "google" {
  region = "europe-west1"
  #version = "~> 3.4"
  alias = "default"
  scopes = [
    "https://www.googleapis.com/auth/cloud-platform",
    "https://www.googleapis.com/auth/userinfo.email",
  ]
}
data "google_service_account_access_token" "default" {
  provider               = google.default
  target_service_account = "terraform-sa@sandbox-bbenlazreg.iam.gserviceaccount.com"
  scopes = [
    "userinfo-email",
    "cloud-platform",
  ]
  lifetime = "1200s"
}
#Provider which use the access_token generated previously
provider "google-beta" {
  #version      = "~> 3.4"
  region       = "europe-west1"
  access_token = data.google_service_account_access_token.default.access_token
  scopes = [
    "https://www.googleapis.com/auth/cloud-platform",
    "https://www.googleapis.com/auth/userinfo.email",
  ]
}