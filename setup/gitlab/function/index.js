var config = {};
var builder = require('gcp-container-builder')(config);

var cloudbuild = Object.create(null);


cloudbuild.images = ['gcr.io/$PROJECT_ID/trigger-container-builder'];

exports.get = (req, res) => {
    var branch = req.ref

    cloudbuild.steps = [
        {
            name: 'gcr.io/cloud-builders/gsutil',
            args: ['cp',
                'gs://trigger-cloud-build/*',
                '.']
        }
        , {
            name: 'gcr.io/cloud-builders/gcloud',
            args: [
                'kms', 
                'decrypt', 
                '--ciphertext-file=.git-credentials.enc', 
                '--plaintext-file=.git-credentials',
                '--location=us-east4',
                '--key=deploy-key',
                '--keyring=gitlab-deploy-key']
        }
          , {
            name: 'gcr.io/cloud-builders/git',
            args: [
              'config',
              '--global',
              'credential.helper',
              '\'store\''],
              env: ['HOME=/workspace']
        }
        , {
            name: 'gcr.io/cloud-builders/git',
              args: [
              'clone',
              '--depth',
              '1',
              'https://gitlab.com/joshlambert/trigger-container-builder.git'],
              env: ['HOME=/workspace']
        }
        , {
            name: 'alpine',
              args: [ '-c',
              'echo "Deploying to branch ${branch}"']
        }];


	builder.createBuild(cloudbuild, function(err, resp) {
	    console.log(err);
	    console.log(resp);
	});

	res.status(200).send('Triggered.');

};