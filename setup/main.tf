resource "google_sourcerepo_repository" "my-repo" {
  name = var.repo_name
  project = var.project
}

resource "google_cloudbuild_trigger" "github-trigger" {
  project = var.project
  trigger_template {
    branch_name = ".*"
    repo_name   = var.repo_name
  }

  filename = "cloudbuild.yaml"
}